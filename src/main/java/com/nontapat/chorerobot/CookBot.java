/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.chorerobot;

/**
 *
 * @author DELL
 */
public class CookBot extends ChoreBot{
    private char n_menu; //1.soup 2.steak 3. salad
    private int dish = 1;
    private char LastMenu = '1';
    public CookBot(String name) {
        super(name);
        System.out.println("CookBot "+name+" is created");
    }
    public String menu(char n_menu){
        
        switch(n_menu){
            case '1':{
                LastMenu = n_menu;
                return "soup";
            }
            case '2':{
                LastMenu = n_menu;
                return "steak";
            }
            case '3':{
                LastMenu = n_menu;
                return "salad";
            } 
            default: 
                break;
        }
        
      return "";
    }
    
    @Override
    public void speak(){
        System.out.println("I am CookBot name: "+name);
    }
    @Override
    public void work(){
        work(LastMenu);
    }
    public void work(int n){
        if(n <= 0){
            System.out.println("Dish must not be 0!!!");
        }else{
            work(LastMenu,n);
        }
    }
    public void work(char m){
        if(m != '1' && m != '2' && m != '3'){
            System.out.println("The selected isn't on menu please select again"
                        +" (1.soup, 2.steak, 3.salad)");
        }else{
            System.out.println("CookBot "+name+" is cooking 1 dish of "+menu(m));
        }
    }
    public void work(char m,int n){
        if(n <= 0){
            System.out.println("Dish must not be 0!!!");
        }else{
            if(m != '1' && m != '2' && m != '3'){
            System.out.println("The selected isn't on menu please select again"
                        +" (1.soup, 2.steak, 3.salad)");
            }else{
            System.out.println("CookBot "+name+" is cooking "+n+" dish of "+menu(m));
            }
        }
    }
    @Override
    public void type() {
        System.out.println(name+" is CookBot");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.chorerobot;

/**
 *
 * @author DELL
 */
public class ChoreBot {
    protected String name;

    public ChoreBot(String name) {
        this.name = name;
        System.out.println("ChoreBot "+name+" is created");
    }
    public void speak(){
        System.out.println("I am ChoreBot name: "+name);
    }
    
    public void work(){
        System.out.println("ChoreBot"+name+" is working");
    }
    
    public void type(){
        System.out.println(name+" is ChoreBot");    
    }
}
